import sqlite3

try:
    con=sqlite3.connect('SlangPanameno.db')
    curs=con.cursor()
    print("Nos conectamos\n")

    #curs.execute('''CREATE TABLE diccionario (
    #                palabra TEXT PRIMARY KEY,
    #               definicion TEXT NOT NULL);''')

    def insertar():
        p=input("\nIngrese una palabra de slang panameño: ")
        d=input(f"Ingrese el significado de '{p}': ")
        qry='''INSERT INTO diccionario 
            ('palabra','definicion')
            VALUES (?, ?)'''
        data=(p,d)
        curs.execute(qry,data)
        print("\nPalabra y definición añadida")

    def editar():
        qry="SELECT palabra FROM diccionario"
        curs.execute(qry)
        data=curs.fetchall()
        print(f"Las palabras para editar son: \n{data}")
        p=input("\nIngrese la palabra que desea cambiar: ")
        qry="SELECT * FROM diccionario WHERE palabra= ?"
        pal=(p,)
        curs.execute(qry,pal)
        data=curs.fetchone()
        if data==None:
            print("\nLa palabra ingresada no se encuentra")
        else:
            pm=input("Ingrese la palbra modificada: ")    
            qry="UPDATE diccionario SET palabra = ? WHERE palabra = ?"
            pal=(pm,p)
            curs.execute(qry,pal)
            print("\nPalabra modificada")

    def eliminar():
        qry="SELECT palabra FROM diccionario"
        curs.execute(qry)
        data=curs.fetchall()
        print(f"Las palabras para eliminar son: \n{data}")
        p=input("\nIngrese la palabra que desea eliminar: ")
        qry="SELECT * FROM diccionario WHERE palabra= ?"
        pal=(p,)
        curs.execute(qry,pal)
        data=curs.fetchone()
        if data==None:
            print("\nLa palabra ingresada no se encuentra")
        else:    
            qry="DELETE FROM diccionario WHERE palabra = ?"
            pal=(p,)
            curs.execute(qry,pal)
            print("\nPalabra eliminada")

    def visualizar():
        qry="SELECT * FROM diccionario"
        curs.execute(qry)
        data=curs.fetchall()
        print(data)

    def buscar():
        p=input("\nIngrese la palabra que desea buscar: ")
        qry="SELECT * FROM diccionario WHERE palabra= ?"
        pal=(p,)
        curs.execute(qry,pal)
        data=curs.fetchone()
        if data==None:
            print("\nLa palabra ingresada no se encuentra")
        else:
            print(data)

    print('''MENU DE OPCIONES\n
            1) Agregar nueva palabra
            2) Editar una palabra existente
            3) Eliminar palabra existente
            4) Ver listado de palabras                
            5) Buscar significado de palabra
            6) Salir''')
    op=int(input("Ingrese la opcion: "))

    if (op==1):
        insertar()
    elif (op==2):
        editar()
    elif (op==3):
        eliminar()
    elif (op==4):
        visualizar()
    elif (op==5):
        buscar()
    elif (op==6):
        print("\nUsted ha salido")
    else:
        print("\nError. Dato no válido") 

    con.commit()
    curs.close()

except sqlite3.Error as Error:
    print("Error mientras se conecta a SQLite ", Error)
finally:
    if(con):
        con.close()
        print("\nConexion cerrada\n")