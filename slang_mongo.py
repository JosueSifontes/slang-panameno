import pymongo
from pymongo import MongoClient
import time

cluster = MongoClient("mongodb+srv://josue:1234@cluster0.hmcbs.mongodb.net/diccionario?retryWrites=true&w=majority")
db = cluster["diccionario"]
collection = db["palabras"]

def insertar():
    p=input("\nIngrese una palabra de slang panameño: ")
    d=input(f"Ingrese la definición de '{p}': ")
    data={"palabra":p, "definicion":d}
    collection.insert_one(data)
    print("Palabra añadida\n")


def editar():
    print("\nLas palabras para editar son: ")
    results = collection.find({},{"_id":0, "definicion":0})
    for result in results:
        print(result)
    p=input("\nIngrese la palabra que desee editar: ")
    aux=1
    resultss = collection.find({"palabra":p},{"_id":0, "definicion":0})
    for result in resultss:
        aux=0
        print(f"\nLa palabra para editar es: {result}")
    if aux==1:
        print(f"\nLa palabra '{p}' no se encuentra")
    else:
        pa=input("\nIngrese la palabra modificada: ")
        act=collection.update_one({"palabra":p},{"$set":{"palabra":pa}})
        print("\nPalabra editada")



def eliminar():
    print("\nLas palabras para eliminar son: ")
    results = collection.find({},{"_id":0, "definicion":0})
    for result in results:
        print(result)
    p=input("\nIngrese la palabra que desee eliminar: ")
    aux=1
    resultss = collection.find({"palabra":p},{"_id":0, "definicion":0})
    for result in resultss:
        aux=0
        print(f"\nLa palabra para eliminar es: {result}")
    if aux==1:
        print(f"\nLa palabra '{p}' no se encuentra")
    else:
        act=collection.delete_one({"palabra":p})
        print("\nPalabra eliminada")

def visualizar():
    results = collection.find({},{"_id":0})
    for result in results:
        print(result)

def buscar():
    p = input("\nIngrese la palabra que desee buscar: ")
    results = collection.find({"palabra":p},{"_id":0})
    aux=1
    for result in results:
        aux=0
        print(result)
    if (aux==1):
        print(f"\nLa palabra '{p}' no se encuentra'")
op=0
while(op!=6):
    print('''\nMENU DE OPCIONES\n
                1) Agregar nueva palabra
                2) Editar una palabra existente
                3) Eliminar palabra existente
                4) Ver listado de palabras                
                5) Buscar significado de palabra
                6) Salir''')
    op=int(input("Ingrese la opcion: "))

    if (op==1):
        insertar()
        time.sleep(2)
    elif (op==2):
        editar()
        time.sleep(2)
    elif (op==3):
        eliminar()
        time.sleep(2)
    elif (op==4):
        visualizar()
        time.sleep(2)
    elif (op==5):
        buscar()
        time.sleep(2)
    elif (op==6):
        print("\nUsted ha salido")
        time.sleep(2)
    else:
        print("\nError. Dato no válido")
        time.sleep(2) 