import requests
import json
import time

def preg1():
    cont=0
    response=requests.get('https://swapi.dev/api/planets/')
    data = response.json()
    aux = data['results']
    for element in aux:
        for k, v in element.items():
            if k=="climate" and v=="arid":
                cont+=1
    print(f"\nEn {cont} pelicula(s) aparecen planetas cuyo clima sea árido\n")

def preg2():
    cont=0
    response=requests.get('https://swapi.dev/api/films/')
    data = response.json()
    aux = data['results']
    for element in aux:
        for k, v in element.items():
            if k=="episode_id" and v==6:
                cont+=1
    print(f"\nEn la sexta película aparecen {cont} Wookies\n")

def preg3():
    response=requests.get('https://swapi.dev/api/starships/')
    data = response.json()
    aux= data['results']
    for element in aux:
        for k, v in element.items():
            if k=="name" and v=="Star Destroyer":
                nave=v
    print(f"\nEl nombre de la aeronave más grande en toda la saga es: {nave}\n")

aux=0
while(aux!=4):
    print('''Menu de Opciones: 
            1. ¿En cuántas películas aparecen planetas cuyo clima sea árido?
            2. ¿Cuántos Wookies aparecen en la sexta película?
            3. ¿Cuál es el nombre de la aeronave más grande en toda la saga?
            4. Salir
            ''')
    aux=int(input("Seleccione una opcion: "))
    #print("\n")
    if aux==1:
        preg1()
        time.sleep(2)
    elif aux==2:
        preg2()
        time.sleep(2)
    elif aux==3:
        preg3()
        time.sleep(2)
    elif aux==4:
        print("\nUsted ha salido")
        time.sleep(2)
    else:
        print("\nError. Número no válido")
        time.sleep(1)

    


