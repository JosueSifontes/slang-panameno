from sqlalchemy import create_engine, Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import exc
import time

try:
    Base = declarative_base()
    engine=create_engine('sqlite:///SlangPanameno.db')
    Base.metadata.create_all(bind=engine)
    Session= sessionmaker(bind=engine)
    session= Session()
    
    
    class Palabra(Base):
        __tablename__ = "diccionario"

        palabra=Column('palabra', String, primary_key=True)
        definicion=Column('definicion', String)
    
    def insertar():
        p=(input("Ingrese una palabra de slang panameño: "))
        d=input(f"Ingrese la definición de {p}: ")
        word =Palabra()
        word.palabra=p
        word.definicion=d
        session.add(word)
        print("\nPalabra y definición añadida")

    def editar():
        r = session.query(Palabra).all()
        print("Las palabras para editar son: ")
        for row in r:
            print(row.palabra)
        p=input("\nIngrese la palabra que desea editar: ")
        act = session.query(Palabra).get(p)
        if act == None:
            print(f"\nLa palabra '{p}' no se encuentra\n")
        else:
            pm=input("Ingrese la palabra modificada: ")
            act.palabra=pm
            print("\nPalabra modificada")
        


    def eliminar():
        r = session.query(Palabra).all()
        print("Las palabras para eliminar son: ")
        for row in r:
            print(row.palabra)
        p=input("\nIngrese la palabra que desea eliminar: ")
        act = session.query(Palabra).get(p)
        if act == None:
            print(f"\nLa palabra '{p}' no se encuentra\n")
        else:
            session.delete(act)
            print("\nPalabra eliminada")

    def visualizar():
        for row in session.query(Palabra, Palabra.palabra, Palabra.definicion).all():
            print(f"[{row.palabra} = {row.definicion}]")

    def buscar():
        p=input("\nIngrese la palabra que desea buscar: ")
        b=session.query(Palabra).filter_by(palabra=p).all()
        aux=1
        for row in b:
            aux=0
            print(f"[{row.palabra} = {row.definicion}]")

        if (aux==1):
            print(f"\nLa palabra '{p}' no se encuentra\n")

    op=0
    while(op!=6):
        print('''MENU DE OPCIONES\n
                1) Agregar nueva palabra
                2) Editar una palabra existente
                3) Eliminar palabra existente
                4) Ver listado de palabras                
                5) Buscar significado de palabra
                6) Salir''')
        op=int(input("Ingrese la opcion: "))

        if (op==1):
            insertar()
            session.commit()
            time.sleep(2)
        elif (op==2):
            editar()
            session.commit()
            time.sleep(2)
        elif (op==3):
            eliminar()
            session.commit()
            time.sleep(2)
        elif (op==4):
            visualizar()
            time.sleep(2)
        elif (op==5):
            buscar()
            time.sleep(2)
        elif (op==6):
            print("\nUsted ha salido")
            time.sleep(2)
        else:
            print("\nError. Dato no válido") 
            time.sleep(2)
    session.close()

except exc.SQLAlchemyError as e:
    print(f"ERROR: {e}")